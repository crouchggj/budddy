#include <stdio.h>
#include <stdlib.h>

#include "buddy.h"

void *test_alloc(uint32_t size)
{
	return malloc(size);
}

void test_free(void *ptr)
{
	free(ptr);
}

void test_allocator(uint32_t size, ret_t *ret)
{
	if (buddy_alloc(size, ret) != NULL) {
		printf("alloc size: 0x%x  ===> actual size: 0x%x, offset: 0x%x, index: %d\r\n",
			size, 1<<ret->list_idx, ret->offset, ret->node_idx);
	} else {
		printf("alloc failure\r\n");
	}
}

int main(int argc, char *argv[])
{
	ret_t ret0, ret1, ret2;

	printf("alloc %d KB for memory management \r\n", 2 << (LEVEL_SIZE - 10));

	buddy_init(test_alloc, test_free);
	
	test_allocator(200, &ret0);
	test_allocator(100, &ret1);
	test_allocator(200, &ret2);

	//buddy_dump();

	buddy_free(&ret0);
	//buddy_free(&ret1);
	buddy_free(&ret2);

	buddy_dump();

	buddy_destroy();

	printf("\r\nfinish allocator!\r\n");
	return 0;
}
