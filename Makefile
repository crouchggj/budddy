.Phony : all clean

all : test

test : test.c buddy.c
	gcc -g -Wall -o $@ $^

clean : 
	rm -rf test test.dSYM
